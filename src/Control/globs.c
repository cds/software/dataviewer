//
// Created by erik.vonreis on 7/2/21.
//

#include <X11/Intrinsic.h>
#include "channel.h"

#ifndef __APPLE__
  char *optarg;
#endif

int  optind, opterr;
char displayIP[80], serverIP[80];
char origDir[1024], iniDir[80];
int  serverPort, webPort, mmMode; /* 0-server, 1-tocfile, 2-frame dir */

char chName[16][MAX_LONG_CHANNEL_NAME_LENGTH], chUnit[16][80], frameFileName[240], restoreFileName[240];
int  restoreFile;
int  chRate[16];

char version_n[8];  /* version number */
char version_m[8], version_y[8];  /* version date */
int  zoomflag, /* -free indicator for grace  */ nolimit;

Atom COMPOUND_TEXT;
Atom import_list[1];