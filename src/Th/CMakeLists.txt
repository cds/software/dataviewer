add_library(datasrv STATIC
        datasrv.c
        daqc_access.c
        )
target_include_directories(datasrv PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}/../Lib
        ${CMAKE_SOURCE_DIR}/advligorts/src/daqd
        ${CMAKE_SOURCE_DIR}/advligorts/src/include
        )
add_library(th::datasrv ALIAS datasrv)
